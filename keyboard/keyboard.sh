#!/bin/bash

# Turn off all
g810-led -a 000000

# Arrows
g810-led -kn h 00ff00 # Set color of a key with no action
g810-led -kn j 00ff00 # Set color of a key with no action
g810-led -kn k 00ff00 # Set color of a key with no action
g810-led -kn l 00ff00 # Set color of a key with no action

# Escape
g810-led -kn esc ff0000 # Set color of a key with no action

# Jump
g810-led -kn w 0000ff # Set color of a key with no action
g810-led -kn b 0000ff # Set color of a key with no action
g810-led -kn e 0000ff # Set color of a key with no action
g810-led -kn shift_right ffff00 # Set color of a key with no action
g810-led -kn shift_left ffff00 # Set color of a key with no action

# Commit changes
g810-led -c

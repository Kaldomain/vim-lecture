class: center, middle, inverse
VIM lecture for InstallFest
===========================
![:scale 30%](images/vim_logo.png)

.footnote[Git repository: [gitlab.com/juhlik/vim-lecture](https://gitlab.com/juhlik/vim-lecture)]

---

class: center, middle, inverse
Cheatsheet
----------

---
class: center, middle
![:scale 90%](images/vim_control.png)  

---

### Closing file
~~`Ctrl` + `c` - "You have no power here!!!"~~  
**`:q` - close VIM**  
`:q!` - force close VIM  
`:x` - save and close VIM  
`:wq` - save and close VIM  
`:wqa` - save and close all tabs VIM  
`ZZ` - save and close VIM  

---

### Global
**`:h keyword` - open help for keyword**  
`Ctrl` + `]` - open link in help  
`:w [filename]` - save file  
`K` - open manual page for word under the cursor

---

### Cursor movement

> Always try to use [number] and commands. For example to move 4 lines down use `4j`.

**`h` - move cursor left**  
**`j` - move cursor down**  
**`k` - move cursor up**  
**`l` - move cursor right**  
**`gh` - move cursor left (don't jump over wrapped line)**  
**`gj` - move cursor down (don't jump over wrapped line)**  
**`gk` - move cursor up (don't jump over wrapped line)**  
**`gl` - move cursor right (don't jump over wrapped line)**  
`H` - move to top of screen  
`M` - move to middle of screen  
`L` - move to bottom of screen  
`zz` - center cursor on screen  
**`w` - jump forwards to the start of a word**  
**`W` - jump forwards to the start of a word (words can contain punctuation)**  
**`e` - jump forwards to the end of a word**  
**`E` - jump forwards to the end of a word (words can contain punctuation)**  
**`b` - jump backwards to the start of a word**  
**`B` - jump backwards to the start of a word (words can contain punctuation)**  

---

`%` - move to matching character (default supported pairs: `()`, `{}`, `[]`  
`^` - jump to the first non-blank character of the line  
`$` - jump to the first non-blank character of the line  
**`*` - jump to the next occurrence of the word under cursor**  
**`gg` - jump to the first line of the document**  
**`G` - jump to the last line of document**  
**`f[character]` - jump to next occurrence of the `[character]`**  
**`F[character]` - jump to previous occurence of the `[character]`**  
`t[character]` - jump one character before next occurrence of the `[character]`  
`T[character]` - jump one character before previous occurrence of the `[character]`  
**`;` - repeat previous `f`, `t`, `F` of `T`**  
**`,` - repeat previous `f`, `t`, `F` of `T`, backwards**  
`}` - jump to next paragraph (or function/block)  
`{` - jump to previous paragraph (or function/block)  
**`Ctrl` + `b` - move back one full screen**  
**`Ctrl` + `f` - move forward one full screen**  
`Ctrl` + `d` - move back 1/2 screen  
`Ctrl` + `u` - move forward 1/2 screen  

---

### Insert mode

> Because `Esc` is used very often, think about swap with `CapsLock`.

**`Esc` - exit insert mode**  
**`i` - insert before the cursor**  
**`I` - insert at the beginning of the line**  
**`a` - insert (append) after the cursor**  
**`A` - insert (append) at the end of the line**  
**`o` - append (open) a new line below the current line**  
**`O` - append (open) a new line above the current line**  
**`ea` - insert (append) at the end of the word**  
**`Ctrl` + `o` - break insert mode for one command**  

---

### Editing

> What about mapping `Ctrl` + `r` to the `U`?

`r` - replace a single character  
`J` - join line below to the current one with one space in between  
`gJ` - join line below to the current one without space in between  
`cc` - change (replace) entire line  
**`cw` - change (replace) to the end of the word**  
**`ciw` - change (replace) the whole word**  
`caw` - change (replace) the whole word with the space at the end  
**`D` - delete from current to the end of the line**  
**`C` - delete from current to the end of the line and jump to insert mode**  
`c$` - change (replace) to the end of the line  
`s` - delete character and substitute text  
`S` - delete line and substitute text (same as cc)  
`xp` - transpose two letters (delete and paste)  
**`u` - undo**  
**`Ctrl` + `r` - redo**  
**`.` - repeat last command**  

---

### Visual mode

**`Esc` - exit visual mode**  
**`v` - start visual mode, mark lines, then do a command (like y-yank)**  
**`V` - start linewise visual mode**  
`o` - move to other end of marked area  
**`Ctrl` + `v` - start visual block mode**  
`O` - move to other corner of block  
`aw` - mark a word  
`ab` - a block with ()  
`aB` - a block with {}  
**`ib` - inner block with ()**  
**`iB` - inner block with {}**  
**`gv` - mark last marked block**  

**`>` - shift text right**  
**`<` - shift text left**  
**`y` - yank (copy) marked text**  
**`d` - delete marked text**  
**`~` - switch case**  

---

### Marks

> Use it. I mean it really, USE IT!  
> Try plugin [vim-signature](https://github.com/kshenoy/vim-signature)

`:marks` - list of marks  
**`ma` - set current position for mark A**  
**`'a` - jump to position of mark A**  
`y'a` - yank text to position of mark A  

---

### Registers
> See [Vim registers tutorial](https://www.brianstorti.com/vim-registers/)

`"[character]y` - yank selected text to [character]  
`"[character]p` - paste [character] register  
`Ctrl` + `r[character]` - paste [character] register in insert mode  
`:reg` - see all the registers and their content  

**Special registers:**
- `""` - last deleted text (with `d`, `c`, `s` or `x`)
- `"[number]` - last 10 yank or deleted text (`"0` the newest, `"9` the oldest)
- `".` - last insert text (read only)
- `"%` - current file path (read only)

---

### Macros

`q[character]` - record macro `[character]`  
`q` - stop recording macro  
`@[character]` - run macro `[character]`  
`@@` - rerun last run macro  

---

### Cut and paste

> As always use `[number]` and command.  
> What about mapping `y$` to the `Y`?  

**`yy` - yank (copy) a line**  
**`yw` - yank (copy) the characters of the word from the cursor position to the start of the next word**  
`y$` - yank (copy) to end of line  
**`p` - put (paste) the clipboard after cursor**  
**`P` - put (paste) before cursor**  
**`dd` - delete (cut) a line**  
**`dw` - delete (cut) the characters of the word from the cursor position to the start of the next word**  
**`dW` - delete (cut) the characters of the word from the cursor position to the start of the next word (words can contain punctuation)**  
**`x` - delete (cut) character**  

---

### Search and replace

> Use `sed` pattern replace as you are used to.  
> You can mark text in Visual mode and then apply replace patter with `:[command]`.  

**`\*` - search for the next occurrence of the word under cursor**
**`/[pattern]` - search for pattern**  
`?[pattern]` - search backward for pattern  
`\[pattern]` - 'very magic' pattern: non-alphanumeric characters are interpreted as special regex symbols (no escaping needed)  
**`n` - repeat search in same direction**  
**`N` - repeat search in opposite direction**  
**`:%s/old/new/g` - replace all old with new throughout file**  
`:%s/old/new/gc` - replace all old with new throughout file with confirmations  
**`:noh` - remove highlighting of search matches**  

---

### Buffers

> Sincerely I have no idea about buffer until this lecture... I'm ashamed.

`:e` file - edit a file in a new buffer  
`:args` - open multiple files in a new buffers  
`:bnext` or :bn - go to the next buffer  
`:bprev` or :bp - go to the previous buffer  
`:bd` - delete a buffer (close a file)  
`:ls` - list all open buffers  

---

### Split window

> Try `set splitbelow` and `set splitright` for more natural split.

**`:sp [file]` - open a file in a new buffer and split window**  
**`:vsp [file]` - open a file in a new buffer and vertically split window**  
`Ctrl` + `ws` - split window  
`Ctrl` + `ww` - switch windows  
`Ctrl` + `wq` - quit a window  
`Ctrl` + `wv` - split window vertically  
**`Ctrl` + `wh` - move cursor to the left window (vertical split)**  
**`Ctrl` + `wl` - move cursor to the right window (vertical split)**  
**`Ctrl` + `wj` - move cursor to the window below (horizontal split)**  
**`Ctrl` + `wk` - move cursor to the window above (horizontal split)**  

---

### Tabs

**`:tabnew [file]` - open a file in a new tab**  
**`Ctrl` + `wT` - move the current split window into its own tab**  
**`gt` - move to the next tab**  
**`gT` - move to the previous tab**  
`:tabmove [number]` - move current tab to the [number]th position (indexed from 0)  
`:tabclose` or `:tabc` - close the current tab and all its windows  
`:tabonly` or `:tabo` - close all tabs except for the current one  
`:tabdo [command]` - run the command on all tabs (e.g. :tabdo q - closes all opened tabs)  

---

### Folding
> See [wiki](http://vim.wikia.com/wiki/Folding)

**`za` - toggle fold (open/close fold)**  
**`zA` - toggle all folds (open/close fold)**  
`zo` - open fold  
`zO` - open all folds  
`zc` - close fold  
`zC` - close all folds  

---

### Commands
> See [commands](https://www.linux.com/learn/vim-tips-working-external-commands)

`:! [command]` - run [command]  
`:r [file]` - read [file] and insert it under cursor  
`:r! [command]` - run [command] and the result insert under cursor  
`% ! [command]` - run command on whole document and replace it with the command result  

### Bonus

`gf` - open file under cursor in new buffer  
`Ctrl` + `wgf` - open file under cursor in new tab  
`Ctrl` + `o` - jump back (from buffer, to last jump point, ...)  
`gg=G` - format/indent document  
`Ctrl` + `a` - Increase number under cursor  
`Ctrl` + `x` - Decrease number under cursor  
`g` + `Ctrl` + `a` - Increase number under cursor (like arithmetic line)  
`g` + `Ctrl` + `x` - Decrease number under cursor (like arithmetic line)

---

### Sources

[Vim logo source](https://commons.wikimedia.org/wiki/File:Vimlogo.svg)  
[Cheatsheet image source](https://edux.fit.cvut.cz/courses/BI-PS1/_detail/tutorials/03/vi.png?id=tutorials%3A03%3Astart)  
[Cheatsheet source](https://vim.rtorr.com/)  

---

class: center, middle, inverse
Brace yourself VIM is coming
-----------------------------

---

Sample text
-----------
> For this you have to install `lynx` previously.

```
:r ! lynx -dump en.wikipedia.org/wiki/Vi
```

---

Task
----

> Try it first with `time nano` and after that with `time vim`.

Edit file `task/main.cpp`

**Instructions:**  
1. Correct indentation.
2. Remove white spaces from the end of the lines.
3. Cut implementation of the `user` lambda function and insert it into `my_print`.
4. Remove `user` lambda function.
5. Change function `user` to `my_print` which is called on the last line of the `main` function.
6. Create new global variable `NEW_GLOBAL` with type `int` and value 5.
7. Change parameter of the calling function `my_print` to `NEW_GLOBAL`.
8. Increase a global variable `NEW_GLOBAL` by 1.

---

### Before
```cpp
#include <iostream>
#include <functional>

const int GLOBAL = 5; 

long my_print(long f_user){ 
}

int main(){
    auto user = [](long loop) -> long {  
    for (long i = 0; i < loop; i++) {               
    for (long j = 0; j < i; j++) {   
    std::cout << "*";   
    } 
    std::cout << std::endl; 
    }

    std::cout << "This is the end!" << std::endl;     

    return loop*loop - 5;    
    }; 

    std::cout << user(GLOBAL) << std::endl;
}
```

---

### After

```cpp
#include <iostream>
#include <functional>

const int GLOBAL = 5;
const int MORE_GLOBAL = 6;

long my_print(long loop){
        for (long i = 0; i < loop; i++) {
            for (long j = 0; j < i; j++) {
                std::cout << "*";
            }
            std::cout << std::endl;
        }

        std::cout << "This is the end!" << std::endl;

        return loop*loop - 5;
}

int main(){
    std::cout << my_print(MORE_GLOBAL) << std::endl;
}
```

---

### One of the possible solution

1. Correct indentation. `gg=G`
2. Remove white spaces from the end of the lines. `:%s/ *$//g`
3. Cut implementation of the `user` lambda function and insert it into `my_print`. `viBd` + `p`
4. Remove `user` lambda function. `/auto` + `2dd`
5. Change function `user` to `my_print` which is called on the last line of the `main` function. `2fucw` + `my_print`
6. Create new global variable `NEW_GLOBAL` with type `int` and value 5. `yyp2wcw` + `NEW_GLOBAL`
7. Change parameter of the calling function `my_print` to `NEW_GLOBAL`. `/GLOBAL` + `.`
8. Increase a global variable `NEW_GLOBAL` by 1. `Ctrl` + `a`

> Personal guess: `vim` time is higher isn't it? Just be cool and practice... Try the same test after month of using VIM.

---

class: center, middle, inverse
One shortcuts set for everything
--------------------------------

---

Application/plugins with Vim-like shortcuts
-------------------------------------------

`vimdiff` - VIM mode showing differences between two, three or four versions of the same file  
`ranger` - terminal file explorer  
`VIMIUM` - browser plugin  
`ideaVIM` - JetBrains plugin  
`overleaf` - online application for editing LaTeX  

---

class: center, middle, inverse
Next lecture???
---------------

---

Possible topics for new lecture
-------------------------------

- Configure your .vimrc  
- Vim plugins  
- Vim and Tex  

---

class: center, middle, inverse
See also
--------

---

Links
-----

[My `.vimrc`](https://gitlab.com/juhlik/laptop-config/home/vimrc)  
[Vim Adventures](https://vim-adventures.com/)  
[Vim jako IDE](https://www.root.cz/serialy/textovy-editor-vim-jako-ide/)  
[Neovim](https://neovim.io/)  
[Remark](https://github.com/gnab/remark)  



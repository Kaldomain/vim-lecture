#include <iostream>
#include <functional>

const int GLOBAL = 5; 

long my_print(long loop){ 
}

int main(){
    auto user = [](long loop) -> long {  
        for (long i = 0; i < loop; i++) {               
        for (long j = 0; j < i; j++) {   
        std::cout << "*";   
        } 
        std::cout << std::endl; 
        }

        std::cout << "This is the end!" << std::endl;     

        return loop*loop - 5;    
    }; 

    std::cout << user(GLOBAL) << std::endl;
}

#include <iostream>
#include <functional>

const int GLOBAL = 5;
const int MORE_GLOBAL = 6;

long my_print(long loop){
        for (long i = 0; i < loop; i++) {
            for (long j = 0; j < i; j++) {
                std::cout << "*";
            }
            std::cout << std::endl;
        }

        std::cout << "This is the end!" << std::endl;

        return loop*loop - 5;
}

int main(){
    std::cout << my_print(MORE_GLOBAL) << std::endl;
}
